<?php
$this->breadcrumbs=array(
	Yii::t('Blog', 'Artikel') => $this->createUrl('/cms/blog/admin'),
	Yii::t('Category', 'Kategorien') => $this->createUrl('/cms/category/admin'),
	Yii::t('Category', 'Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Category', 'Kategorie erstellen'),
		'icon' => 'plus',
	)
);
 
$this->renderPartial(
	'/category/_form', 
	array(
		'model' => $model,
	)
);

$this->endWidget();