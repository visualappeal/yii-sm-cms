<?php
$this->breadcrumbs = array(
	Yii::t('Blog', 'Artikel') => $this->createUrl('/cms/blog/admin'),
	Yii::t('Category', 'Kategorien') => $this->createUrl('/cms/category/admin'),
	$model->title => $this->createUrl('/cms/category/viewTitle', array('title' => $model->slug)),
	Yii::t('Category', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->title,
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'/category/_form', 
	array(
		'model' => $model,
	)
);

$this->endWidget();