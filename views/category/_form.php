<?php
$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'blog-form',
		'horizontal' => true,
	)
); 
?>

	<?php echo $form->beginControlGroup($model, 'title'); ?>
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->beginControls($model, 'title'); ?>
			<?php echo $form->textField($model, 'title'); ?>
		<?php echo $form->endControls($model, 'title'); ?>
	<?php echo $form->endControlGroup($model, 'title'); ?>

	<?php echo $form->beginControlGroup($model, 'slug'); ?>
		<?php echo $form->labelEx($model, 'slug'); ?>
		<?php echo $form->beginControls($model, 'slug'); ?>
			<?php echo $form->textField($model, 'slug', array('class' => 'slug', 'data-title' => 'Category_title')); ?>
		<?php echo $form->endControls($model, 'slug'); ?>
	<?php echo $form->endControlGroup($model, 'slug'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Category', 'Kategorie erstellen') : Yii::t('Category', 'Kategorie speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>