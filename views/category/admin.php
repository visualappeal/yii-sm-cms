<?php
$this->breadcrumbs=array(
	Yii::t('Blog', 'Artikel') => $this->createUrl('/cms/blog/admin'),
	Yii::t('Cms.Category', 'Kategorien'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Category', 'Kategorien'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Cms.Category', 'erstellen'), array('/cms/category/create'), '', 'mini', '', 'plus', false, array('title' => Yii::t('Category', 'Kategorie erstellen')))
		),
		'icon' => 'th-large',
		'table' => true,
	)
);

$this->widget('EBootstrapGridView', array(
	'id' => 'cms-category-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'title',
		array(
			'class' => 'EBootstrapButtonColumn',
		),
	),
	'pager' => array(
		'class' => 'EBootstrapLinkPager',
		'header' => false,
	),
	'pagerAlign' => 'right',
)); 

$this->endWidget();