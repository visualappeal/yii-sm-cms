<div class="cms post clearfix" id="post-<?php echo $data->id; ?>">
	<h3><?php echo EBootstrap::link($data->title, $this->createUrl('/cms/blog/viewTitle', array('title' => $data->slug))); ?></h3>
	<div class="cms-meta post-meta">
		<p><?php echo Yii::t('CMS', 'Geschrieben am {date} von {user} in {category}', array(
			'{date}' => strftime('%d. %B %G', strtotime($data->created)),
			'{user}' => isset($data->theUser) ? EBootstrap::link($data->theUser->name, array('/user/user/profile', 'id' => $data->theUser->id)) : '<em>'.Yii::t('CMS', 'Benutzer gelöscht').'</em>',
			'{category}' => (isset($data->theCategory->title)) ? EBootstrap::link($data->theCategory->title, array('/cms/category/viewTitle', 'title' => $data->theCategory->slug)) : '<em>'.Yii::t('CMS', 'Kategorie gelöscht').'</em>',
		)) ?></p>
	</div>
	<div class="cms-content post-content clearfix">
		<?php echo $data->teaser; ?>
	</div>
</div>