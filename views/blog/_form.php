<?php
$cs = Yii::app()->clientScript;

//Datetime-Picker
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.cms.js').'/timepicker.js');
$cs->registerScriptFile($jsFile, CClientScript::POS_END);

$cs->registerScript('cms-datepicker', "
$.timepicker.regional['de'] = {
	timeOnlyTitle: 'Uhrzeit auswählen',
	timeText: 'Zeit',
	hourText: 'Stunde',
	minuteText: 'Minute',
	secondText: 'Sekunde',
	currentText: 'Jetzt',
	closeText: 'Auswählen',
	ampm: false,
	dateFormat: 'dd.mm.yy',
	timeFormat: 'hh:mm:ss'
};

$.timepicker.setDefaults($.timepicker.regional['de']);

$('.datetimepicker').datetimepicker();
");

//Categories
$cs->registerScript('category-autocomplete', '
var categories = '.$model->loadJsonCategories().';

$(".category-autocomplete").autocomplete({
	source: categories,
});
', CClientScript::POS_READY);

//Editor
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.min.js');
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/de.js');
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/fullscreen.js');
$cs->registerCssFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.css');

$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'cms-blog-form',
	)
);

echo $form->errorSummary($model);
?>

<div class="row">
	<div class="col-sm-9">
		<?php echo $form->beginControlGroup($model, 'title', array('class' => 'no-border no-padding-bottom')); ?>
			<?php echo $form->labelEx($model, 'title'); ?>
			<?php echo $form->beginControls($model, 'title'); ?>
				<?php echo $form->textField($model, 'title', array('class' => 'span12')); ?>
				<?php echo $form->error($model, 'title'); ?>
			<?php echo $form->endControls($model, 'title'); ?>
		<?php echo $form->endControlGroup($model, 'title'); ?>
	
		<?php echo $form->beginControlGroup($model, 'slug', array('class' => 'no-border no-padding')); ?>
			<?php echo $form->labelEx($model, 'slug'); ?>
			<?php echo $form->beginControls($model, 'slug'); ?>
				<?php echo $form->textField($model, 'slug', array('class' => 'span12 slug', 'data-title' =>  ($model->type == Cms::TYPE_POST) ? 'Post_title' : 'Page_title')); ?>
				<?php echo $form->error($model, 'slug'); ?>
			<?php echo $form->endControls($model, 'slug'); ?>
		<?php echo $form->endControlGroup($model, 'slug'); ?>

		<?php echo $form->beginControlGroup($model, 'content', array('class' => 'no-border')); ?>
			<?php echo $form->beginControls($model, 'content'); ?>
				<?php echo $form->textArea($model, 'content', array('class' => 'span12 texteditor', 'rows' => 20)); ?>
				<?php echo $form->error($model, 'content'); ?>
			<?php echo $form->endControls($model, 'content'); ?>
		<?php echo $form->endControlGroup($model, 'content'); ?>

		<?php echo $form->beginActions(); ?>
			<?php if ($model->status != Cms::STATUS_PUBLISHED): ?>
				<?php echo EBootstrap::submitButton(Yii::t('CMS', 'Speichern'), '', '', false, 'ok', false, array('name' => 'save')); ?> 
				<?php echo EBootstrap::submitButton(Yii::t('CMS', 'Veröffentlichen'), 'success', '', false, 'plus', true, array('name' => 'publish')); ?>
			<?php else: ?>
				<?php echo EBootstrap::submitButton(Yii::t('CMS', 'Speichern'), 'success', '', false, 'ok', true, array('name' => 'save')); ?> 
			<?php endif; ?>
		<?php echo $form->endActions(); ?>
	</div>

	<div class="col-sm-3">
		<?php echo $form->beginControlGroup($model, 'the_tags'); ?>
			<?php echo $form->labelEx($model, 'the_tags'); ?>
			<?php echo $form->beginControls($model, 'the_tags'); ?>
				<?php
				$tags = $model->tags;
				if (is_array($model->tags))
					$tags = implode(', ', $model->tags);
				else
					$tags = '';

				$this->widget('CAutoComplete', array(
					'name' => 'Cms[the_tags]',
					'value' => $tags,
					'url'=> $this->createUrl('/cms/tag/autocomplete'),
					'multiple' => true,
					'mustMatch' => false,
					'matchCase' => false,
					'cssFile' => false,
					'selectFirst' => true,
					'htmlOptions' => array(
						'class' => 'span12',
					)
				)); 
				?>	
				<?php echo $form->error($model, 'the_tags'); ?>
				<p class="help-block"><?php echo Yii::t('CMS', 'Mehrere Stichwörter müssen mit einem Komma getrennt werden ("Auto, Rot, VW Golf")') ?></p>
			<?php echo $form->endControls($model, 'the_tags'); ?>
		<?php echo $form->endControlGroup($model, 'the_tags'); ?>

		<?php echo $form->beginControlGroup($model, 'category'); ?>
			<?php echo $form->labelEx($model, 'category'); ?>
			<?php echo $form->beginControls($model, 'category'); ?>
				<?php 
				if (isset($model->theCategory) and (isset($model->theCategory->title))) {
					$theCategory = $model->theCategory->title;
				}
				else {
					$theCategory = '';
				}
				?>
				<?php echo $form->textField($model, 'category', array('class' => 'span12 category-autocomplete', 'value' => $theCategory)); ?>
				<?php echo $form->error($model, 'category'); ?>
				<?php $this->renderPartial('/cms/_categories', array('categories' => $categories, 'active' => $model->category_id)); ?>
			<?php echo $form->endControls($model, 'category'); ?>
		<?php echo $form->endControlGroup($model, 'category'); ?>
		
		<?php echo $form->beginControlGroup($model, 'created'); ?>
			<?php echo $form->labelEx($model, 'created'); ?>
			<?php echo $form->beginControls($model, 'created'); ?>
				<?php echo $form->textField($model, 'created', array('class' => 'span12 datetimepicker')); ?>
				<?php echo $form->error($model, 'created'); ?>
			<?php echo $form->endControls($model, 'created'); ?>
		<?php echo $form->endControlGroup($model, 'created'); ?>
		
		<?php echo $form->beginControlGroup($model, 'updated'); ?>
			<?php echo $form->labelEx($model, 'updated'); ?>
			<?php echo $form->beginControls($model, 'updated'); ?>
				<?php echo $form->textField($model, 'updated', array('disabled' => '1', 'class' => 'span12')); ?>
				<?php echo $form->error($model, 'updated'); ?>
			<?php echo $form->endControls($model, 'updated'); ?>
		<?php echo $form->endControlGroup($model, 'updated'); ?>
		
		<?php echo $form->beginControlGroup($model, 'status'); ?>
			<?php echo $form->labelEx($model, 'status'); ?>
			<?php echo $form->beginControls($model, 'status'); ?>
				<?php echo $form->dropDownList($model, 'status', Cms::getStatuses(), array('class' => 'span12')); ?>
				<?php echo $form->error($model, 'status'); ?>
			<?php echo $form->endControls($model, 'status'); ?>
		<?php echo $form->endControlGroup($model, 'status'); ?>
	</div>
</div>

<?php $this->endWidget(); ?>