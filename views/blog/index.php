<?php 
$this->title[] = Yii::t('CMS', 'Blog');

$this->breadcrumbs = array(
	Yii::t('CMS', 'Blog'),
);
?>

<?php if (!isset($category)): ?>
	<h2><?php echo Yii::t('CMS', 'Blog'); ?></h2>
<?php else: ?>
	<h2><?php echo Yii::t('CMS', 'Blog - {category}', array('{category}' => $category->title)); ?></h2>
<?php endif; ?>

<?php 
$this->widget(
	'EBootstrapListView', 
		array(
		'dataProvider' => $posts,
	    'itemView'=>'/blog/_view',
	    'ajaxUpdate' => false,
	    'summaryText' => false,
	    'pager' => array(
	    	'class' => 'EBootstrapLinkPager',
	    	'header' => false,
	    ),
	)
);
?>