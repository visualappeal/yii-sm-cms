<?php
$this->title[] = $model->title;

$this->breadcrumbs=array(
	Yii::t('CMS', 'Blog') => $this->createUrl('/blog'),
	$model->title,
);
?>

<h2><?php echo $model->title; ?></h2>

<div class="cms post" id="post-<?php echo $model->id; ?>">
	<div class="cms-meta post-meta">
		<p><?php echo Yii::t('CMS', 'Geschrieben am {date} von {user} in {category}', array(
			'{date}' => strftime('%d. %B %G', strtotime($model->created)),
			'{user}' => isset($model->theUser) ? EBootstrap::link($model->theUser->name, array('/user/user/profile', 'id' => $model->theUser->id)) : '<em>'.Yii::t('CMS', 'Benutzer gelöscht').'</em>',
			'{category}' => (isset($model->theCategory->title)) ? EBootstrap::link($model->theCategory->title, array('/cms/category/viewTitle', 'title' => $model->theCategory->slug)) : '<em>'.Yii::t('CMS', 'Kategorie gelöscht').'</em>',
		)) ?></p>
	</div>
	<div class="cms-content post-content">
		<?php echo $model->content; ?>
	</div>
	<?php if (Yii::app()->user->isBoard()): ?>
	<div class="cms-actions post-actions">
		<ul class="unstyled">
			<li><?php echo EBootstrap::link(Yii::t('CMS', '{icon} Bearbeiten', array('{icon}' => '<i class="icon icon-pencil"></i>')), $this->createUrl('/cms/blog/update', array('id' => $model->id))); ?></li>
		</ul>
	</div>
	<?php endif; ?>
	<div class="cms-comment post-comment">
		<div class="cms-comments post-comments">
			<h2><?php echo Yii::t('CMS', 'Kommentare') ?></h2>
			<?php foreach ($model->comments as $c): ?>
				<?php
				$this->renderPartial('/comment/_view', array(
					'data' => $c,
				));
				?>
			<?php endforeach; ?>
		</div>
		<div class="cms-comment-form post-comment-form">
			<?php 
				$this->renderPartial('/comment/_form', array(
					'model' => $comment,
				));
			?>
		</div>
	</div>
</div>