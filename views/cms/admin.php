<?php
$this->title[] = ($model->type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel') : Yii::t('CMS', 'Seiten');
$this->title[] = Yii::t('Core', 'Administration');

$this->breadcrumbs=array(
	($type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel') : Yii::t('CMS', 'Seiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => ($type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel Verwalten') : Yii::t('CMS', 'Seiten Verwalten'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('CMS', 'Erstellen'), ($type == Cms::TYPE_POST) ? $this->createUrl('/cms/blog/create') : $this->createUrl('/cms/page/create'), '', 'mini', false, 'plus', false, array('title' => ($type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel schreiben') : Yii::t('CMS', 'Seite erstellen')))
		),
		'icon' => 'th-list',
		'table' => true,
	)
);

function getCmsRowCssClass($this, $row, $data) {
	switch ($data->status) {
		case Cms::STATUS_PUBLISHED:
			return 'success';
		case Cms::STATUS_REVIEW:
			return 'info';
		case Cms::STATUS_PRIVATE:
			return 'warning';
		default:
			return '';
	}
}

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'cms-grid',
		'dataProvider' => $model->search($type),
		'filter' => $model,
		'columns' => array(
			'id',
			array(
				'name' => 'title',
				'value' => '$data->shortTitle',
			),
			array(
				'name' => 'created',
				'value' => 'date("d.m.Y H:i", strtotime($data->created))',
				'filter' => false,
			),
			array(
				'class'=>'EBootstrapButtonColumn',
			),
		),
		'pager' => array(
			'class' => 'EBootstrapLinkPager',
			'header' => false,
		),
		'pagerAlign' => 'right',
		'rowCssClassExpression' => 'getCmsRowCssClass($this, $row, $data)',
	)
);

$this->endWidget();