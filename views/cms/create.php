<?php
$type = ($model->type == Cms::TYPE_POST) ? 'blog' : 'page';

$this->breadcrumbs=array(
	($model->type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel') : Yii::t('CMS', 'Seiten') => $this->createUrl('/cms/'.$type.'/admin'),
	Yii::t('CMS', 'Schreiben'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => ($model->type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel schreiben') : Yii::t('CMS', 'Seite schreiben'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'/cms/_form', 
	array(
		'model' => $model,
		'categories' => $categories,
	)
);

$this->endWidget();