<?php 

Yii::app()->clientScript->registerScript('cms-category-choose', '
	$(function() {
		$("#cms-category-choose li a").click(function() {
			console.log($(this).html());
			$(".category-autocomplete").val($(this).html());
		});
	});
');

?>

<p><?php echo Yii::t('CMS', 'Neue Kategorie eingeben oder vorhandene auswählen:'); ?></p>

<ul id="cms-category-choose">
	<?php foreach ($categories as $category): ?>
		<li><a href="#"<?php if ($active == $category->id): ?> class="bold"<?php endif; ?>><?php echo $category->title; ?></a></li>
	<?php endforeach; ?>
</ul>