<?php

$this->title[] = $model->title;
$this->title[] = Yii::t('CMS', 'bearbeiten');
$this->title[] = Yii::t('Core', 'Administration');

$type = ($model->type == Cms::TYPE_POST) ? 'blog' : 'page';

$this->breadcrumbs = array(
	($model->type == Cms::TYPE_POST) ? Yii::t('CMS', 'Artikel') : Yii::t('CMS', 'Seiten') => $this->createUrl('/cms/'.$type.'/admin'),
	$model->title => $this->createUrl('/cms/'.$type.'/viewTitle', array('title' => $model->slug)),
	Yii::t('CMS', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->title,
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'/cms/_form', 
	array(
		'model' => $model,
		'categories' => $categories,
	)
);

$this->endWidget();