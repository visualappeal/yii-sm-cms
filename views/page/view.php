<?php
$this->title[] = $model->title;

$this->breadcrumbs=array(
	$model->title,
);
?>

<h2><?php echo $model->title; ?></h2>

<div class="cms page" id="page-<?php echo $model->id; ?>">
	<div class="cms-content post-content">
		<?php echo $model->content; ?>
	</div>
	<?php if (Yii::app()->user->isBoard()): ?>
	<div class="cms-actions post-actions">
		<ul class="unstyled">
			<li><?php echo EBootstrap::link(Yii::t('CMS', 'Bearbeiten'), $this->createUrl('/cms/page/update', array('id' => $model->id))); ?></li>
		</ul>
	</div>
	<?php endif; ?>
</div>