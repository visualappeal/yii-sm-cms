<?php

$this->title[] = Yii::t('Comment', 'Kommentare');
$this->title[] = Yii::t('Core', 'Administration');

$this->breadcrumbs = array(
	Yii::t('Blog', 'Artikel') => $this->createUrl('/cms/blog/admin'),
	Yii::t('Comment', 'Kommentare'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Comment', 'Kommentare (offen)'),
		'buttons' => EBootstrap::buttonGroup(
			array(
				//EBootstrap::ibutton()
			)
		),
		'icon' => 'comment',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
		array(
		'id' => 'comment-grid',
		'dataProvider' => $dataProvider,
		'columns' => array(
			'id',
			array(
				'name' => 'user_id',
				'type' => 'raw',
				'value' => '(isset($data->theUser)) ? EBootstrap::link($data->theUser->username, array("/user/user/view", "id" => $data->theUser->id)) : EBootstrap::encode($data->name)',
			),
			array(
				'name' => 'post_id',
				'type' => 'raw',
				'value' => 'EBootstrap::link($data->thePost->title, array("/cms/blog/viewTitle", "title" => $data->thePost->slug))',
			),
			array(
				'name' => 'created',
				'value' => 'date("d.m.Y H:i:s", strtotime($data->created))',
				'filter' => false,
			),
			array(
				'class' => 'EBootstrapButtonColumn',
				'template' => '{view}{delete}{approve}',
				'viewButtonUrl' => 'Yii::app()->createUrl("/cms/blog/viewTitle", array("title" => $data->thePost->slug, "#" => "comment-".$data->id))',
				'buttons' => array(
					'approve' => array(
						'imageUrl' => false,
						'url' => 'Yii::app()->createUrl("/cms/comment/approve", array("id" => $data->id))',
						'label' => '<i class="icon icon-ok"></i>'
					),
				),
			),
		),
		'pager' => array(
			'class' => 'EBootstrapLinkPager',
			'header' => false,
		),
		'pagerAlign' => 'right',
	)
); 

$this->endWidget();