<div class="comment-entry" id="comment-<?php echo $data->id; ?>">

	<h3>
	<?php if (isset($data->theUser)): ?>
		<?php echo EBootstrap::link($data->theUser->name, array('/user/user/view', 'id' => $data->theUser->id)); ?>
	<?php else: ?>
		<?php echo (empty($data->website)) ? EBootstrap::encode($data->name) : EBootstrap::link($data->name, $data->website); ?>
	<?php endif; ?>
		<small><?php echo strftime('%e. %B %G %H:%M', strtotime($data->created)); ?></small>
	</h3>

	<div class="comment-content">
		<?php echo $data->htmlContent; ?>
	</div>
	
</div>