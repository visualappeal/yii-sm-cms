<h3><?php echo Yii::t('Comment', 'Kommentar schreiben'); ?></h3>

<?php
$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'comment-form',
		'horizontal' => true,
	)
);
?>

	<?php if (Yii::app()->user->isGuest): ?>

		<?php echo $form->bootstrapTextField($model, 'name'); ?>

		<?php echo $form->bootstrapTextField($model, 'email'); ?>
		
		<?php echo $form->bootstrapTextField($model, 'website'); ?>

	<?php else: ?>
		
		<p><?php echo Yii::t('Comment', 'Du schreibst als <em>{user}</em>', array('{user}' => Yii::app()->user->name)); ?></p>

	<?php endif; ?>

	<?php echo $form->bootstrapTextArea($model, 'content'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Comment', 'Kommentar schreiben') : Yii::t('Comment', 'Speichern'), 'success'); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>