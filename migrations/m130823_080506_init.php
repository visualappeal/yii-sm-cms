<?php

/**
 * Erste Migration um notwendige Tabellen zu erstellen.
 */
class m130823_080506_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{cms_category}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(50) NOT NULL',
				'slug' => 'varchar(50) NOT NULL',
				'PRIMARY KEY (`id`)',
			)
		);

		$this->createTable(
			'{{cms_post}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'user_id' => 'int(10) unsigned NOT NULL',
				'title' => 'varchar(100) NOT NULL',
				'slug' => 'varchar(100) NOT NULL',
				'content' => 'text NOT NULL',
				'category_id' => 'int(10) unsigned NOT NULL',
				'type' => 'tinyint(3) unsigned NOT NULL',
				'status' => 'tinyint(3) unsigned NOT NULL',
				'created' => 'datetime NOT NULL',
				'updated' => 'datetime NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `user_id` (`user_id`)'
			)
		);

		$this->createTable(
			'{{cms_comment}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'post_id' => 'int(10) unsigned NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'name' => 'varchar(75) NOT NULL',
				'email' => 'varchar(100) NOT NULL',
				'website' => 'varchar(100) NOT NULL',
				'content' => 'text NOT NULL',
				'created' => 'datetime NOT NULL',
				'parent_id' => 'int(10) unsigned NOT NULL',
				'approved' => 'tinyint(1) NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `post_id` (`post_id`)',
				'KEY `user_id` (`user_id`)',
			)
		);

		$this->createTable(
			'{{cms_tag}}',
			array(
				'id' => 'mediumint(8) unsigned NOT NULL AUTO_INCREMENT',
				'name' => 'varchar(100) NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'tag_count' => 'int(10) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'UNIQUE KEY `name` (`name`)',
				'KEY `user_id` (`user_id`)',
			)
		);

		$this->createTable(
			'{{cms_tag_post}}',
			array(
				'tag_id' => 'mediumint(8) unsigned NOT NULL',
				'post_id' => 'int(10) unsigned NOT NULL',
				'PRIMARY KEY (`tag_id`,`post_id`)',
			)
		);

		return true;
	}

	public function safeDown()
	{
		$this->dropTable('{{cms_tag_post}}');
		$this->dropTable('{{cms_tag}}');
		$this->dropTable('{{cms_comment}}');
		$this->dropTable('{{cms_post}}');
		$this->dropTable('{{cms_category}}');

		return true;
	}
}