<?php

/**
 * CMS Module
 *
 * @package CMS
 */
class CmsModule extends CWebModule
{
	/**
	 * Init application.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init()
	{
		Yii::import('application.modules.cms.models.*');
		Yii::import('application.modules.cms.portlets.*');

		return parent::init();
	}
}

?>