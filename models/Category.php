<?php

class Category extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cms_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title', 'required'),
			array('title, slug', 'length', 'max' => 50),
			array('id, title', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'posts' => array(self::HAS_MANY, 'Post', 'category_id'),
			'pages' => array(self::HAS_MANY, 'Page', 'category_id'),
		);
	}

	/**
	 * Get the attribute labels
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('CMS', 'ID'),
			'title' => Yii::t('CMS', 'Titel'),
			'slug' => Yii::t('CMS', 'Permalink'),
		);
	}
	
	/**
	 * Category default scope.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);

		return array(
			'order' => "$t.title ASC",
		);
	}
    
    /**
     * Category behaviors. Creates a slug for the category.
     *
     * @access public
     * @return array
     */
	public function behaviors() {
		return array(
			'sluggable' => array(
				'class' => 'application.modules.cms.extensions.behaviors.SluggableBehavior.SluggableBehavior',
				'columns' => array('title'),
				'unique' => true,
				'update' => false,
			),
		);
	}

	/**
	 * Retrieves a list of posts based on the current search/filter conditions.
	 *
	 * @access public
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$t = $this->getTableAlias();

		$criteria = new CDbCriteria;

		$criteria->compare("$t.id", $this->id);
		$criteria->compare("$t.title", $this->title, true);

		return new CActiveDataProvider(
			$this, 
			array(
				'criteria' => $criteria,
			)
		);
	}
}