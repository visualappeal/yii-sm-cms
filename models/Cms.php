<?php

/**
 * CMS is the parent class for posts and pages.
 *
 * @package CMS
 */
class Cms extends CActiveRecord
{
	const STATUS_PRIVATE = 0;
	const STATUS_PUBLISHED = 1;
	const STATUS_REVIEW = 2;
	
	const TYPE_POST = 0;
	const TYPE_PAGE = 1;
	
	/**
	 * The category. Is either an existing category or a new.
	 *
	 * @access public
	 * @var string
	 */
	public $category;
	
	/**
	 * Tags for this page. Are either existing, new or mixed tags sepreated through ", "
	 *
	 * @access public
	 * @var string
	 */
	public $the_tags;

	/**
	 * Array of valid image mime types for the image uploader.
	 *
	 * @access public
	 * @var array
	 */
	public static $IMAGE_TYPES = array(
		'image/png',
		'image/jpg',
		'image/jpeg',
		'image/gif'
	);

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className (default: __CLASS__)
	 *
	 * @access public
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the table name for CMS.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cms_post}}';
	}

	/**
	 * Get the validation rules for the model.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, title, created', 'required'),
			array('updated', 'required', 'on' => 'updated'),
			
			array('user_id, category_id, type, status', 'numerical', 'integerOnly' => true),
			array('title, slug', 'length', 'max' => 100),
			array('category', 'length', 'max' => 50),
			array('created, updated', 'date', 'allowEmpty' => true, 'format' => 'yyyy-M-d H:m:s'),
			
			array('category, content, the_tags', 'safe'),
			array('id, title', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Get the model relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
			'theCategory' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id'),
		);
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('CMS', 'ID'),
			'user_id' => Yii::t('CMS', 'Benutzer'),
			'title' => Yii::t('CMS', 'Titel'),
			'content' => Yii::t('CMS', 'Inhalt'),
			'slug' => Yii::t('CMS', 'Permalink'),
			'category' => Yii::t('CMS', 'Kategorie'),
			'category_id' => Yii::t('CMS', 'Kategorie'),
			'created' => Yii::t('CMS', 'Erstellt'),
			'updated' => Yii::t('CMS', 'Zuletzt bearbeitet'),
			'type' => Yii::t('CMS', 'Typ'),
			'the_tags' => Yii::t('CMS', 'Stichwörter'),
			'status' => Yii::t('CMS', 'Status'),
		);
	}

	/**
	 * Get the default scope for the CMS.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope()
	{
		$t = $this->getTableAlias(false, false);

		return array(
			'order' => "$t.`created` DESC",
		);
	}
	
	/**
	 * Get the scopes for the CMS.
	 *
	 * @access public
	 * @return array
	 */
	public function scopes()
	{
		$t = $this->getTableAlias();

		return array(
			'private'=>array(
				'condition' => "$t.`status` = :status",
				'params' => array(
					':status' => self::STATUS_PRIVATE,
				),
			),
			'published'=>array(
				'condition' => "$t.`status` = :status",
				'params' => array(
					':status' => self::STATUS_PUBLISHED,
				),
			),
			'post'=>array(
				'condition' => "$t.`type` = :type",
				'params' => array(
					':type' => self::TYPE_POST,
				),
			),
			'page'=>array(
				'condition' => "$t.`type` = :type",
				'params' => array(
					':type' => self::TYPE_PAGE,
				),
			),
		);
	}

	/**
	 * Scope to get CMS pages for a category.
	 *
	 * @param integer $categoryId ID of the category
	 *
	 * @access public
	 * @return Cms
	 */
	public function showCategory($categoryId)
	{
		$t = $this->getTableAlias();

		$this->getDbCriteria()->mergeWith(
			array(
				'condition' => "$t.category_id = :category_id",
				'params' => array(
					':category_id' => $categoryId,
				),
			)
		);

		return $this;
	}
	
	/**
	 * Before validate method. Set the category and tags.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate() {
		if (parent::beforeValidate()) {
			//Set category
			if (!empty($this->category)) {
				$category = Category::model()->findByAttributes(array(
					'title' => $this->category,
				));
				
				if (is_null($category)) {
					$category = new Category;
					$category->title = $this->category;
					$category->save();
				}
				$this->category_id = $category->id;
			}
			
			//Set tags
			$this->setTags($this->the_tags);
			
			//Set dates
			if (!$this->isNewRecord)
				$this->updated = date('Y-m-d H:i:s');

			if (strtotime($this->created) !== false)
				$this->created = date('Y-m-d H:i:s', strtotime($this->created));

			//Remove unknown HTML tags from content
			$this->content = strip_tags($this->content, '<code><span><div><label><a><br><p><b><i><del><strike><u><img><video><audio><iframe><object><embed><param><blockquote><mark><cite><small><ul><ol><li><hr><dl><dt><dd><sup><sub><big><pre><code><figure><figcaption><strong><em><table><tr><td><th><tbody><thead><tfoot><h1><h2><h3><h4><h5><h6>');
		}
		
		return parent::beforeValidate();
	}
	
	/**
	 * After construct method. Set user ID and creation date.
	 *
	 * @access public
	 * @return void
	 */
	public function afterConstruct() {
		$this->user_id = Yii::app()->user->id;
		$this->created = date('Y-m-d H:i:s');

		return parent::afterConstruct();
	}
	
	/**
	 * After find method. Convert the dates to a local format.
	 *
	 * @access public
	 * @return void
	 */
	public function afterFind() {   	
		if ($this->updated == '0000-00-00 00:00:00')
			$this->updated = '';
		else
			$this->updated = date('d.m.Y H:i:s', strtotime($this->updated));

		$this->created = date('d.m.Y H:i:s', strtotime($this->created));

		return parent::afterFind();
	}
	
	/**
	 * Get a teaser for the CMS page.
	 *
	 * @access public
	 * @return string
	 */
	public function getTeaser() {
		$teaserBreak = preg_split('/<div.*page-break.*>.*<\/div>/', $this->content);

		if (count($teaserBreak) !== count($this->content)) {
			$teaser = $teaserBreak[0];
			$type = ($this->type == self::TYPE_POST) ? 'blog' : 'page';
			$teaser .= EBootstrap::tag('p', array(), EBootstrap::link(Yii::t('CMS', 'Weiterlesen…'), Yii::app()->createAbsoluteUrl('/cms/'.$type.'/viewTitle', array('title' => $this->slug))));
		}
		else
			$teaser = $this->content;
		
		return $teaser;
	}
	
	/**
	 * Get CMS behaviors.
	 *
	 * @access public
	 * @return array
	 */
	public function behaviors() {
		return array(
			'sluggable' => array(
				'class' => 'application.modules.cms.extensions.behaviors.SluggableBehavior.SluggableBehavior',
				'columns' => array('title'),
				'unique' => true,
				'update' => false,
			),
			'sluggable' => array(
				'class' => 'application.modules.cms.extensions.behaviors.TaggableBehavior.ETaggableBehavior',
				'tagTable' => '{{cms_tag}}',
				'tagBindingTable' => '{{cms_tag_post}}',
				'modelTableFk' => 'post_id',
				'tagTablePk' => 'id',
				'tagTableName' => 'name',
				'tagTableCount' => 'tag_count',
				'tagBindingTableTagId' => 'tag_id',
				'createTagsAutomatically' => true,
				'insertValues' => array(
					'user_id' => Yii::app()->user->id,
				),
			),
		);
	}
	
	/**
	 * Get a shortend title.
	 *
	 * @access public
	 * @return string
	 */
	public function getShortTitle() {
		$wordLimit = 5;
		$teaser = ''; 
		
		$words = explode(' ',$this->title); 
		$i = 0; 
		while(($i < $wordLimit) and (isset($words[$i]))) { 
			$teaser .= $words[$i]." ";
			$i++;
		}
		
		$wordCount = str_word_count($this->title, 0);
		$teaserWordCount = str_word_count($teaser, 0);
		if ($wordCount > $teaserWordCount)
			$teaser .= '...';
		return $teaser;
	}
	
	/**
	 * Get all categories for the typeahead
	 *
	 * @access public
	 * @return JSON Array
	 */
	public function loadJsonCategories() {
		$models = Category::model()->findAll();

		$categories = array();
		foreach ($models as $model) {
			$categories[] = $model->title;
		}
		
		return CJSON::encode($categories);
	}

	public static function getStatuses()
	{
		return array(
			self::STATUS_PRIVATE => Yii::t('CMS', 'Privat'),
			self::STATUS_PUBLISHED => Yii::t('CMS', 'Öffentlich'),
			self::STATUS_REVIEW => Yii::t('CMS', 'Revision'),
		);
	}
	
	/**
	 * Retrieves a list of posts based on the current search/filter conditions.
	 *
	 * @param integer $type Type of the CMS page
	 *
	 * @access public
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($type = self::TYPE_POST)
	{
		$t = $this->getTableAlias();
		$criteria = new CDbCriteria;

		$criteria->compare("$t.id", $this->id);
		$criteria->compare("$t.title", $this->title, true);
		$criteria->compare("$t.type", $type);

		return new CActiveDataProvider(
			$this, 
			array(
				'criteria' => $criteria,
			)
		);
	}
}