<?php 

class Page extends Cms {
	/**
	 * Page default scope.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);

		return array(
			'condition' => "$t.type = :type",
			'limit' => 10,
			'order' => "$t.created DESC",
			'with' => 'theCategory',
			'params' => array(
				':type' => self::TYPE_PAGE,
			),
		);
	}

	/**
	 * After construct method. Set the page type
	 *
	 * @access public
	 * @return void
	 */
	public function afterConstruct()
	{
		$this->type = self::TYPE_PAGE;

		return parent::afterConstruct();
	}
}

?>