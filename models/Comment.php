<?php

/**
 * This is the model class for the comments.
 *
 * The followings are the available columns in table '{{posts_comments}}':
 * @property integer $id ID of the comment
 * @property integer $post_id ID of the post the comment belongs to
 * @property integer $user_id ID of the user who wrote the comment (if logged in)
 * @property string $name Name of the user (if not logged in)
 * @property string $email Email of the user (if not logged in)
 * @property string $website Website of the user (if not logged in)
 * @property string $content Content of the comment
 * @property string $created Date of creation
 * @property integer $parent_id ID of the parent comment
 * @property boolean $approved Whether the comment is approved
 *
 * @package CMS
 * @subpackage Comment
 */
class Comment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the model table name.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cms_comment}}';
	}

	/**
	 * Get the attribute validaton rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('post_id, content, created', 'required'),
			array('user_id', 'required', 'on' => 'registered'),
			array('name, email', 'required', 'on' => 'guest'),
			
			array('user_id, post_id, parent_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max' => 75),
			array('email', 'length', 'max' => 100),
			array('website', 'length', 'max' => 100),
			
			array('approved', 'boolean'),
			array('website', 'url'),

			array('user_id', 'unsafe'),
			array('id, post_id, content', 'safe'),
			array('name, email, website', 'safe', 'on' => 'guest'),
		);
	}

	/**
	 * Get the comment relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'thePost' => array(self::BELONGS_TO, 'Post', 'post_id'),
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
			'theParent' => array(self::BELONGS_TO, 'Comment', 'parent_id'),
		);
	}
	
	/**
	 * Get the comment default scope.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		
		if (Yii::app()->user->isBoard()) {
			return array(
				'order' => "$t.created DESC",
			);
		}
		else {
			return array(
				'condition' => "$t.approved = :approved",
				'order' => "$t.created DESC",
				'params' => array(
					':approved' => true,
				),
			);
		}
	}
	
	/**
	 * Get the comment scopes.
	 *
	 * @access public
	 * @return array
	 */
	public function scopes() {
		$t = $this->getTableAlias();
		
		return array(
			'approved' => array(
				'condition' => "$t.approved = :approved",
				'params' => array(
					':approved' => true,
				),
			),
		);
	}
	
	/**
	 * Get the HTML content for this comment.
	 *
	 * @access public
	 * @return string
	 */
	public function getHtmlContent() {
		return nl2br($this->content);
	}

	/**
	 * Create empty comment for given post.
	 *
	 * @access public
	 * @return Comment
	 */
	public static function createForPost(Cms $post)
	{
		if (!Yii::app()->user->isGuest) {
			$comment = new Comment('registered');
		}
		else {
			$comment = new Comment('guest');
		}

		$comment->post_id = $post->id;
		return $comment;
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Comment', 'ID'),
			'post_id' => Yii::t('Comment', 'Artikel'),
			'user_id' => Yii::t('Comment', 'Benutzer'),
			'name' => Yii::t('Comment', 'Name'),
			'email' => Yii::t('Comment', 'E-Mail'),
			'website' => Yii::t('Comment', 'Internetseite'),
			'content' => Yii::t('Comment', 'Inhalt'),
			'created' => Yii::t('Comment', 'Erstellt'),
			'parent_id' => Yii::t('Comment', 'Übergeordnet'),
			'approved' => Yii::t('Comment', 'Genehmigt'),
		);
	}
	
	/**
	 * Before validate method. Set approved status for the comment.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate() {
		if (parent::beforeValidate() and $this->isNewRecord) {
			$this->created = date('Y-m-d H:i:s');
			
			if (!Yii::app()->user->isGuest) {
				$this->user_id = Yii::app()->user->id;
				$this->approved = true;
			}
			else {
				$comment = Comment::model()->findByAttributes(
					array(
						'email' => $this->email,
						'approved' => true,
					)
				);
				
				if (!is_null($comment))
					$this->approved = true;
				else
					$this->approved = false;
				
				if (strpos($this->website, 'http://') === false)
					$this->website = 'http://' . $this->website;
			}
		}
		
		return parent::beforeValidate();
	}
}