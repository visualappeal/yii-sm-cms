<?php 

class Post extends Cms {
	/**
	 * Default scope for posts.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		
		return array(
			'condition' => "$t.type = :type",
			'limit' => 10,
			'order' => "$t.created DESC",
			'with' => array('theCategory', 'theUser'),
			'params' => array(
				':type' => self::TYPE_POST,
			),
		);
	}

	/**
	 * After construct method. Set the page type
	 *
	 * @access public
	 * @return void
	 */
	public function afterConstruct()
	{
		$this->type = self::TYPE_POST;

		return parent::afterConstruct();
	}

	/**
	 * Get the last time a post changed.
	 *
	 * @access public
	 * @return integer timestamp
	 */
	public static function updatedTime()
	{
		$command = Yii::app()->db->createCommand(
			'SELECT
				GREATEST(created, updated) as `ts`
			FROM
				{{cms_post}}
			WHERE
				`type` = :type 
				AND `status` = :status;'
		);

		$command->bindValue(':type', self::TYPE_POST);
		$command->bindValue(':status', self::STATUS_PUBLISHED);
		$result = $command->queryRow();

		if (isset($result['ts'])) {
			return strtotime($result['ts']);
		} else {
			return time();
		}
	}
}

?>