<?php

/**
 * Comment controller.
 *
 * @package CMS
 * @subpackage Comment
 */
class CommentController extends Controller
{
	/**
	 * Default action.
	 *
	 * @access public
	 * @var string
	 */
	public $defaultAction = 'admin';

	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Get the access rules.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin', 'delete', 'approve'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
		
	/**
	 * Manage not approved comments.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$dataProvider = new CActiveDataProvider(
			'Comment', 
			array(
				'criteria' => array(
					'condition' => 'approved = :approved',
					'params' => array(
						'approved' => false,
					),
				),
			)
		);
		
		$this->render(
			'/comment/admin', 
			array(
				'dataProvider' => $dataProvider,
			)
		);
	}
	
	/**
	 * Delete comment.
	 *
	 * @param integer $id Comment ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			$model = Comment::model()->findByPk($id)->delete();

			if(!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400);
		}
	}
	
	/**
	 * Approve comment.
	 *
	 * @param integer $id Comment ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionApprove($id) {
		$model = Comment::model()->findByPk($id);
		
		if (!$model->approved) {
			$model->approved = true;
			if ($model->save()) {
				Yii::app()->user->setFlash('info', Yii::t('Comment', 'Kommentar genehmigt.'));
			}
			else
				throw new CHttpException(500);
		}
		else {
			Yii::app()->user->setFlash('warning', Yii::t('Comment', 'Kommentar wurde schon genehmigt!'));
		}
		
		$this->redirect(array('/cms/comment/admin'));
	}
}