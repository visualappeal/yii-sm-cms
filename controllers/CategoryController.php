<?php

/**
 * Controller for CMS categories.
 *
 * @package CMS
 * @subpackage Category
 */
class CategoryController extends Controller
{
	/**
	 * Get the default action.
	 *
	 * @access public
	 * @var string
	 */
	public $defaultAction = 'admin';

	/**
	 * Get the default layout
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Get controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Get controller access rules
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('view', 'viewTitle'),
				'users'=> array('*'),
			),
			array('allow',
				'actions'=>array('admin', 'create', 'update', 'delete'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
		
	/**
	 * Manage cms categories.
	 *
	 * @access public
	 */
	public function actionAdmin()
	{
		$model = new Category('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['Category']))
			$model->attributes = $_GET['Category'];
		
		$this->render(
			'/category/admin', 
			array(
				'model' => $model,
			)
		);
	}
	
	/**
	 * View category by ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id, $title = null) {
		$this->layout = '//layouts/column1';
		
		if (is_null($title))
			$category = Category::model()->findByPk($id);
		else
			$category = Category::model()->findByAttributes(
				array(
					'slug' => $title,
				)
			);
		
		if (is_null($category))
			throw new CHttpException(404, Yii::t('Category', 'Die Kategorie konnte nicht gefunden werden!'));

		$posts = new CActiveDataProvider(
			'Post',
			array(
				'criteria' => array(
					'scopes' => array(
						'published',
						'showCategory' => $category->id,
					)
				),
			)
		);
		
		$this->render(
			'/blog/index', 
			array(
				'category' => $category,
				'posts' => $posts,
			)
		);
	}
	
	/**
	 * View category by title
	 *
	 * @param string $title Category title
	 *
	 * @access public
	 * @return void
	 */
	public function actionViewTitle($title) {
		$this->actionView(0, $title);
	}
	
	/**
	 * Delete category.
	 *
	 * @param integer $id Category ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest) {
			$model = Category::model()->findByPk($id)->delete();

			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400);
		}
	}
	
	/**
	 * Create new category.
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate() {
		$model = new Category;
		
		if (isset($_POST['Category'])) {
			$model->attributes = $_POST['Category'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Category', 'Neue Kategorie `{title}` erstellt.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/category/admin'));
			}
		}
		
		$this->render(
			'/category/create', 
			array(
				'model' => $model,
			)
		);
	}
	
	
	/**
	 * Edit category.
	 *
	 * @param intgeger $id Category ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id) {
		$model = Category::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (isset($_POST['Category'])) {
			$model->attributes = $_POST['Category'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Category', 'Kategorie `{title}` bearbeitet.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/category'));
			}
		}
		
		$this->render(
			'/category/update', 
			array(
				'model' => $model,
			)
		);
	}
}