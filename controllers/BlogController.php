<?php

/**
 * Controller for blog posts.
 *
 * @package CMS
 * @subpackage Blog
 */
class BlogController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Get the access rules.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'view', 'viewTitle'),
				'users' =>  array('*'),
			),
			array('allow',
				'actions' => array('admin', 'create', 'update', 'delete', 'publish'),
				'roles'  =>  array(User::LEVEL_BOARD),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Display meta information for the admin above the post.
	 *
	 * @param Post Model
	 *
	 * @access public
	 * @return void
	 */
	public function showFlashs($model) {
		switch ($model->status) {
			case Cms::STATUS_PRIVATE:
			case Cms::STATUS_REVIEW:
				$html = Yii::t('CMS', '{heading-start}Vorschau{heading-end}Dieser Artikel ist noch nicht veröffentlicht. Nur Administratoren können diesen Artikel sehen.', array(
					'{heading-start}' => '<h4 class="alert-heading">',
					'{heading-end}' => '</h4><p>',
				));
				$html .= '</p><p>';
				$html .= EBootstrap::ibutton(Yii::t('CMS', 'Freigeben'), array('/cms/blog/publish', 'id' => $model->id), 'success', '', false, 'globe', true)." ";
				$html .= EBootstrap::ibutton(Yii::t('CMS', 'Bearbeiten'), array('/cms/blog/update', 'id' => $model->id), '', '', false, 'pencil');
				$html .= '</p>';
				
				Yii::app()->user->setFlash('block-info', $html);
				break;
		}
	}
	
	/**
	 * Display the latest posts.
	 *
	 * @access public
	 * @return void
	 */
	public function actionIndex() {
		$this->layout = '//layouts/column2';
		
		$posts = new CActiveDataProvider('Post', array(
			'criteria' => array(
				'with' => array(
					'theUser',
					'theCategory',
				),
				'scopes' => array(
					'post',
				),
			),
			'pagination' => array(
				'pageSize' => 10,
			),
		));
		
		$this->render('index', array(
			'posts' => $posts,
		));
	}
	
	/**
	 * View post by ID.
	 *
	 * @param integer $id Post ID
	 * @param string $title Display by title (Default: null)
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id, $title = null) {
		$this->layout = '//layouts/column2';
		
		if (is_null($title))
			$model = Post::model()->findByPk($id);
		else
			$model = Post::model()->findByAttributes(
				array(
					'slug' => $title,
				)
			);
		
		if (is_null($model))
			throw new CHttpException(404, Yii::t('Post', 'Der Beitrag konnte leider nicht gefunden werden!'));
			
		$this->showFlashs($model);
		
		//Create empty comment for the post
		$comment = Comment::createForPost($model);
		
		if (isset($_POST['Comment'])) {
			$comment->attributes = $_POST['Comment'];
			
			if ($comment->save()) {
				if (!$comment->approved) {
					Yii::app()->user->setFlash('info', Yii::t('CMS', 'Dein Kommentar wurde gespeichert muss aber noch von einem Mitarbeiter genehmigt werden. Melde dich an damit dein Kommentar automatisch veröffentlicht wird.'));
				}
				else {
					Yii::app()->user->setFlash('success', Yii::t('CMS', 'Dein Kommentar wurde veröffentlicht.'));
				}
				
				$comment = Comment::createForPost($model);
			}
		}
		
		$this->render('view', array(
			'model' => $model,
			'comment' => $comment,
		));
	}
	
	/**
	 * View post by title
	 *
	 * @param string $title Post title
	 *
	 * @access public
	 * @return void
	 */
	public function actionViewTitle($title) {
		$this->actionView(0, $title);
	}
	
	/**
	 * Publish post.
	 *
	 * @param integer $id Post ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionPublish($id) {
		$model = Post::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if ($model->status != Cms::STATUS_PUBLISHED) {
			$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Der Artikel `{title}` wurde freigegeben.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/blog/viewTitle', 'title' => $model->slug));
			}
			else {
				throw new CHttpException(500);
			}
		}
		else {
			Yii::app()->user->setFlash('warning', Yii::t('CMS', 'Artikel `{title}` kann nicht freigegeben werden da er schon freigegeben ist!', array('{title}' => $model->title)));
			$this->redirect(array('/cms/blog/update', 'id' => $model->id));
		}
	}
	
	/**
	 * Manage posts.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Post('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['Post']))
			$model->attributes = $_GET['Post'];
		
		$this->render(
			'/cms/admin', 
			array(
				'model' => $model,
				'type' => Cms::TYPE_POST,
			)
		);
	}
	
	/**
	 * Delete post.
	 *
	 * @param integer $id Post ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			$model = Post::model()->findByPk($id)->delete();

			if(!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400);
		}
	}
	
	/**
	 * Create new post.
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate() {
		$model = new Post;
		
		if (isset($_POST['Post'])) {
			$model->attributes = $_POST['Post'];

			if (isset($_POST['publish']))
				$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Neuen Artikel `{title}` erstellt.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/blog/viewTitle', 'title' => $model->slug));
			}
		}
		
		$categories = Category::model()->findAll();
		
		$this->render(
			'/cms/create', 
			array(
				'model' => $model,
				'categories' => $categories,
			)
		);
	}
	
	/**
	 * Edit post.
	 *
	 * @param integer $id Post ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id) {
		$model = Post::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];

			if (isset($_POST['publish']))
				$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Artikel `{title}` bearbeitet.', array('{title}' => $model->title)));
			}
		}
		
		$categories = Category::model()->findAll();
		
		$this->render(
			'/cms/update', 
			array(
				'model' => $model,
				'categories' => $categories,
			)
		);
	}
}