<?php 

/**
 * Feed controller.
 *
 * @package CMS
 * @subpackage Feed
 */
class FeedController extends Controller {	
	/**
	 * Generates a RSS feed.
	 *
	 * @access public
	 * @return void
	 */
	public function actionRss() {
		Yii::import('application.modules.cms.extensions.feed.*');

		$criteria = new CDbCriteria;
		$criteria->limit = 10;
		$models = Cms::model()->post()->with('theUser')->findAll($criteria);
		
		$feed = new EFeed();
		
		$feed->title = Yii::app()->name . ' ' . Yii::t('CMS', 'RSS Feed');
		$feed->description = Yii::t('CMS', 'Die neusten Beiträge des CVJM Langenberg');
		$feed->addChannelTag('language', 'de-de');
		$feed->addChannelTag('pubDate', date(DATE_RSS, Post::updatedTime()));
		$feed->addChannelTag('link', Yii::app()->createAbsoluteUrl('/'));
		
		foreach ($models as $model) {
			$item = $feed->createNewItem();
			$item->title = $model->title;
			$item->link = Yii::app()->createAbsoluteUrl('/cms/blog/viewTitle', array('title' => $model->slug));
			$item->date = date('c', strtotime($model->created));
			$item->description = $model->teaser;
			
			if (isset($model->theUser->id)) {
				$item->addTag('author', $model->theUser->email . '(' . $model->theUser->name . ')');
			}
			
			$item->addTag('guid', Yii::app()->createAbsoluteUrl('/cms/blog/view', array('id' => $model->id)), array('isPermaLink' => 'true'));
			
			$feed->addItem($item);
		}
		
		$feed->generateFeed();
		Yii::app()->end();
	}
	
	/**
	 * Generates an Atom feed.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAtom() {
		Yii::import('application.modules.cms.extensions.feed.*');

		$criteria = new CDbCriteria;
		$criteria->limit = 10;
		$models = Cms::model()->post()->with('theUser')->findAll($criteria);
		
		$feed = new EFeed(EFeed::ATOM);
		
		$feed->title = Yii::app()->name . ' ' . Yii::t('CMS', 'RSS Feed');
		$feed->link = Yii::app()->createAbsoluteUrl('/');
		$feed->addChannelTag('updated', gmdate('Y-m-d\TH:i:s\Z', Post::updatedTime()));
		$feed->addChannelTag('author', array('name' => 'CVJM Langenberg', 'uri' => Yii::app()->createAbsoluteUrl('/cms/page/viewTitle', array('title' => 'impressum'))));
		
		foreach ($models as $model) {
			$item = $feed->createNewItem();
			$item->title = $model->title;
			$item->link = Yii::app()->createAbsoluteUrl('/cms/blog/viewTitle', array('title' => $model->slug));
			$item->date = gmdate('Y-m-d\TH:i:s\Z', strtotime($model->created));
			$item->description = $model->teaser;
			
			$feed->addItem($item);
		}
		
		$feed->generateFeed();
		Yii::app()->end();
	}
	
}

?>