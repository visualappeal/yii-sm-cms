<?php

class CmsController extends Controller
{
	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Get the access rules.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('upload'),
				'users' =>  array('@'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * Upload a file to the cms.
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpload()
	{
		$dir = Yii::app()->basePath . '/../uploads/images/';
		$type = strtolower($_FILES['file']['type']);

		if (in_array($type, Cms::$IMAGE_TYPES)) {
			$filename = md5_file($_FILES['file']['tmp_name'])  . '_' . $_FILES['file']['name'];
			if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir . $filename)) {
				$image = Yii::app()->simpleImage->load($dir . $filename);
				$image->resizeToWidth(1280);
				$image->save($dir . $filename);

				$response = array(
					'status' => 'ok',
					'filelink' => Yii::app()->request->baseUrl . '/uploads/images/' . $filename,
				);
			} else {
				$response = array(
					'status' => 'error',
					'message' => Yii::t('Cms.Upload', 'Die Datei konnte nicht gespeichert werden.'),
				);
			}
		} else {
			$response = array(
				'status' => 'error',
				'message' => Yii::t('Cms.Upload', 'Die Datei ist kein Bild und kann nicht hochgeladen werden.'),
			);
		}

		echo CJSON::encode($response);
	}
}