<?php

class TagController extends Controller
{	
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('autocomplete'),
				'users'=> array('*'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Autocomplete the tags in the cms form
	 *
	 * @access public
	 * @return void
	 */
	public function actionAutocomplete() {
		if (isset($_GET['q']) and (!empty($_GET['q']))) {
			$criteria = new CDbCriteria;
			$criteria->compare('`name`', $_GET['q'], true);
			
			$models = Cms::model()->getAllTags($criteria);
			$tags = '';
			foreach ($models as $model)
				$tags .= $model . "\n";
			
			echo $tags;
			exit;
		}
	}
}

?>