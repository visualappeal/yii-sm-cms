<?php

/**
 * Controller for blog posts.
 *
 * @package CMS
 * @subpackage Page
 */
class PageController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Get the access rules.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('view', 'viewTitle'),
				'users' =>  array('*'),
			),
			array('allow',
				'actions' => array('admin', 'create', 'update', 'delete', 'publish'),
				'roles'  =>  array(User::LEVEL_BOARD),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Display meta information for the admin above the page.
	 *
	 * @param Page Model
	 *
	 * @access public
	 * @return void
	 */
	public function showFlashs($model) {
		switch ($model->status) {
			case Cms::STATUS_PRIVATE:
			case Cms::STATUS_REVIEW:
				$html = Yii::t('CMS', '{heading-start}Vorschau{heading-end}Dieser Artikel ist noch nicht veröffentlicht. Nur Administratoren können diesen Artikel sehen.', array(
					'{heading-start}' => '<h4 class="alert-heading">',
					'{heading-end}' => '</h4><p>',
				));
				$html .= '</p><p>';
				$html .= EBootstrap::ibutton(Yii::t('CMS', 'Freigeben'), array('/cms/page/publish', 'id' => $model->id), 'success', '', false, 'globe', true)." ";
				$html .= EBootstrap::ibutton(Yii::t('CMS', 'Bearbeiten'), array('/cms/page/update', 'id' => $model->id), '', '', false, 'pencil');
				$html .= '</p>';
				
				Yii::app()->user->setFlash('block-info', $html);
				break;
		}
	}
	
	/**
	 * View page by ID.
	 *
	 * @param integer $id Page ID
	 * @param string $title Display by title (Default: null)
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id, $title = null) {
		$this->layout = '//layouts/column1';
		
		if (is_null($title))
			$model = Page::model()->findByPk($id);
		else
			$model = Page::model()->findByAttributes(
				array(
					'slug' => $title,
				)
			);
		
		if (is_null($model))
			throw new CHttpException(404, Yii::t('Page', 'Die Seite konnte leider nicht gefunden werden!'));
		
		$this->showFlashs($model);
		
		$this->render(
			'view', 
			array(
				'model' => $model,
			)
		);
	}
	
	/**
	 * View page by title
	 *
	 * @param string $title Page title
	 *
	 * @access public
	 * @return void
	 */
	public function actionViewTitle($title) {
		$this->actionView(0, $title);
	}
	
	/**
	 * Publish a page.
	 *
	 * @param integer $id page ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionPublish($id) {
		$model = Page::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if ($model->status != Cms::STATUS_PUBLISHED) {
			$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Seite "{title}" veröffentlicht.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/page/viewTitle', 'title' => $model->slug));
			}
			else {
				throw new CHttpException(500);
			}
		}
		else {
			Yii::app()->user->setFlash('warning', Yii::t('CMS', 'Artikel "{title}" kann nicht veröffentlich werden da sie schon veröffentlicht ist!', array('{title}' => $model->title)));
			$this->redirect(array('/cms/page/update', 'id' => $model->id));
		}
	}
	
	/**
	 * Manage pages.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Page('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['Page']))
			$model->attributes = $_GET['Page'];
		
		$this->render(
			'/cms/admin', 
			array(
				'model' => $model,
				'type' => Cms::TYPE_PAGE,
			)
		);
	}
	
	/**
	 * Delete page.
	 *
	 * @param integer $id Post ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			$model = Post::model()->findByPk($id)->delete();

			if(!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400);
		}
	}
	
	/**
	 * Create new page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate() {
		$model = new Page;
		
		if (isset($_POST['Page'])) {
			$model->attributes = $_POST['Page'];

			if (isset($_POST['publish']))
				$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Neue Seite "{title}" erstellt.', array('{title}' => $model->title)));
				$this->redirect(array('/cms/page/viewTitle', 'title' => $model->slug));
			}
		}
		
		$categories = Category::model()->findAll();
		
		$this->render(
			'/cms/create', 
			array(
				'model' => $model,
				'categories' => $categories,
			)
		);
	}
	
	/**
	 * Edit page.
	 *
	 * @param integer $id Page ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id) {
		$model = Page::model()->page()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (isset($_POST['Cms'])) {
			$model->attributes = $_POST['Cms'];

			if (isset($_POST['publish']))
				$model->status = Cms::STATUS_PUBLISHED;
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('CMS', 'Seite "{title}" bearbeitet.', array('{title}' => $model->title)));
			}
		}
		
		$categories = Category::model()->findAll();
		
		$this->render(
			'/cms/update', 
			array(
				'model' => $model,
				'categories' => $categories,
			)
		);
	}
}