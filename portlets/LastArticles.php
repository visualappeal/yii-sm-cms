<?php

Yii::import('zii.widgets.CPortlet');

class LastArticles extends CPortlet {
	public $title = 'Letzte Artikel';
	public $count = 10;
	
	protected function renderContent() {
		$models = Cms::model()->post()->findAll(array(
			'limit' => $this->count,
		));

		echo EBootstrap::openTag('ul');
		
		foreach ($models as $model) {
			echo EBootstrap::tag('li', array(), EBootstrap::link($model->title, array('/cms/blog/viewTitle', 'title' => $model->slug)));
		}
		
		echo EBootstrap::closeTag('ul');
	}
}

?>