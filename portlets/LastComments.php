<?php

Yii::import('zii.widgets.CPortlet');

class LastComments extends CPortlet {
	public $title = 'Letzte Kommentare';
	public $count = 10;
	
	protected function renderContent() {
		$models = Comment::model()->approved()->findAll(array(
			'limit' => $this->count,
			'with' => array(
				'thePost',
			),
		));

		echo EBootstrap::openTag('ul');
		
		foreach ($models as $model) {
			if (isset($model->theUser->id)) {
				$title = $model->theUser->name . ' (' . $model->thePost->title . ')';
			}
			elseif (!empty($model->name)) {
				$title = $model->name . ' (' . $model->thePost->title . ')';
			}
			else {
				$title = $model->thePost->title;
			}
			echo EBootstrap::tag('li', array(), EBootstrap::link($title, array('/cms/blog/viewTitle', 'title' => $model->thePost->slug, '#' => 'comment-'.$model->id)));
		}
		
		echo EBootstrap::closeTag('ul');
	}
}

?>